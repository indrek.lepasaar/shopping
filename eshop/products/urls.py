from django.urls import path
from .views import home, product_detail, add_to_cart, remove_from_cart, view_cart, increase_cart_item, decrease_cart_item


urlpatterns = [
    path('', home, name="home"),
    path('<int:pk>/', product_detail, name="product_detail"),
    path('add-to-cart/<int:product_id>/', add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<int:product_id>/', remove_from_cart, name='remove-from-cart'),
    path('cart/', view_cart, name='cart'),
    path('increase-cart-item/<int:product_id>/', increase_cart_item, name='increase-cart-item'),
    path('decrease-cart-item/<int:product_id>/', decrease_cart_item, name='decrease-cart-item'),
    # path('post/new/', PostCreateView.as_view(), name="post_create"),
    # path('post/<int:pk>/edit/', PostUpdateView.as_view(), name="post_update"),
    # path('post/<int:pk>/delete/', PostDeleteView.as_view(), name="post_delete"),
]