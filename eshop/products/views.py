from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from .models import *
# from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

# Create your views here.

# def index_page(request):
#     return render(request, 'index.html')


def home(request):
    products = Products.objects.all()
    products = Products.objects.order_by('name')
    context = {'products': products}
    return render(request, 'index.html', context)


def product_detail(request, pk):
    # Retrieve the Post object
    product = get_object_or_404(Products, pk=pk)

    # Context data for rendering the template
    context = {
        'product': product,
        'description': product.description,
        'price': product.price,
        'image': product.image,
    }

    return render(request, 'item.html', context)


@login_required(login_url='login')
def add_to_cart(request, product_id):
    product = Products.objects.get(pk=product_id)
    cart, created = Cart.objects.get_or_create(user=request.user)
    cart_item, item_created = CartItem.objects.get_or_create(cart=cart, product=product)
    
    if not item_created:
        cart_item.quantity += 1
        cart_item.save()
    
    return redirect('home')


@login_required(login_url='login')
def remove_from_cart(request, product_id):
    product = Products.objects.get(pk=product_id)
    cart = Cart.objects.get(user=request.user)
    try:
        cart_item = cart.cartitem_set.get(product=product)
        if cart_item.quantity >= 1:
             cart_item.delete()
    except CartItem.DoesNotExist:
        pass
    
    return redirect('cart')


@login_required(login_url='login')
def view_cart(request):
    cart = request.user.cart
    cart_items = CartItem.objects.filter(cart=cart)
    return render(request, 'cart.html', {'cart_items': cart_items})


@login_required(login_url='login')
def increase_cart_item(request, product_id):
    product = Products.objects.get(pk=product_id)
    cart = request.user.cart
    cart_item, created = CartItem.objects.get_or_create(cart=cart, product=product)

    cart_item.quantity += 1
    cart_item.save()

    return redirect('cart')

@login_required(login_url='login')
def decrease_cart_item(request, product_id):
    product = Products.objects.get(pk=product_id)
    cart = request.user.cart
    cart_item = cart.cartitem_set.get(product=product)

    if cart_item.quantity > 1:
        cart_item.quantity -= 1
        cart_item.save()
    else:
        cart_item.delete()

    return redirect('cart')

# class HomeView(ListView):
#     model = Products
#     template_name = 'index.html'
#     context_object_name = 'products'
#     ordering = ['name']

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['featured_posts'] = FeaturedPost.objects.all().order_by('priority')
#         visits_count = self.request.session.get('visits_count', 0)
#         self.request.session['visits_count'] = visits_count + 1
#         context['count'] = visits_count
#         return context