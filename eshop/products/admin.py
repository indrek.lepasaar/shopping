from django.contrib import admin
from .models import Category, Products, Cart, Order, CartItem

# Register your models here.
admin.site.register(Category)
admin.site.register(Products)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(CartItem)