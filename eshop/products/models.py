from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name
    

class Products(models.Model):
    name = models.CharField(max_length=255)
    price = models.IntegerField(default=0)
    image = models.ImageField(blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="products", null=True, blank=True)
    description = models.TextField(null=True)
                                   
    def __str__(self):
        return f"{self.image} - {self.name} - {self.price}"


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    products = models.ManyToManyField(Products, through='CartItem')
    create_at = models.DateField(auto_now_add=True)
    
    def __str__(self):
        return f"Shopping cart for:{self.user.username}"
    

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, primary_key=True)
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
    ordered_date = models.DateField(auto_now_add=True)
    total_price = models.IntegerField()
    
    def __str__(self):
        return f"{self.user} - {self.cart} - {self.ordered_date} - {self.total_price}"
    
    
class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    
    def __str__(self):
        return f"Products in your cart: {self.product} x {self.quantity}"
    
    
User.profile = property(lambda u: User.objects.get_or_create(user=u)[0])
User.cart = property(lambda u: Cart.objects.get_or_create(user=u)[0])